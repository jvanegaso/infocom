Los siguientes son los pasos para la instalacion, configuracion y utilizacion un servidor de correo electronico, servidor web.

1- Instalacion del sistema operativo en servidor.
  
  Se selecciona el medio y sistema operativo, en nuestro caso se utilizara un debian 10, en una maquina virtual creada en VirtualBox 6.0.
  
  1.1- Los siguientes son los pasos para la creacion de la maquina virtual.
  
  1.1.1- Se seleciona el botom new, para iniciar la creacion de la nueva maquina.
    
  ![Comando tree](BotonNew_VB.png "Comando tree")

  1.1.2- Se le selecciona nombre, Sistema Operativo a la maquina, ademas de la ruta donde se guardara.
    
  ![Comando tree](NombreMaquinaVB.png "Comando tree")

  1.1.3- Se selecciona la cantidad de memoria RAM para la maquina.
  
  ![Comando tree](RAM_VB.png "Comando tree")

  1.1.4- Se seleccionan opciones de Disco Duro,se selecciona boton Create.
   
  ![Comando tree](HardDisk_VB.png "Comando tree")

  ![Comando tree](HardDisk2_VB.png "Comando tree")

  ![Comando tree](HardDisk3.png "Comando tree")

  ![Comando tree](HardDisk4.png "Comando tree")

  1.1.5- Ahora selecionamos el boton Settings.

  ![Comando tree](BotonNew_VB.png "Comando tree")

  1.1.6- Vamos a storage y agregamos un nuevo optical drive. En add selecionamos la imagen ISO del sistema operativo, damos OK.

  ![Comando tree](OpticalDrive1_VB.png "Comando tree")  

  ![Comando tree](OpticalDrive2_VB.png "Comando tree")  

  1.1.7- En network, seleccionar el adaptador como bridged adapter, para que tome una ip privada.

  ![Comando tree](red_VB.png "Comando tree")
  
  1.1.8- Seleccionamos el boton start, para iniciar la maquina virtual creada e instalar el sistema operativo.

  ![Comando tree](BotonNew_VB.png "Comando tree")

  1.2- Los siguientes son los pasos para configurar debian 10 "Buster".

  1.2.1- Se selecciona el metodo de instalacion.

  ![Comando tree](GraficalSelection_Debian.png "Comando tree")

  1.2.2- Se selecciona el lenguaje.
   
  ![Comando tree](lenguaje_Debian.png "Comando tree")

  1.2.3- Se selecciona formato.
  
  ![Comando tree](Formato_Debian.png "Comando tree")

  1.2.4- Se seleciona configuracion del teclado.
   
  ![Comando tree](Keyboard_debian.png "Comando tree")
  
  1.2.5- Se ingresa el hostname de la maquina.

  ![Comando tree](Hostname_Debian.png "Comando tree")

  1.2.6- Se ingresa nombre de dominio.

  ![Comando tree](nombredominio_Debian.png "Comando tree")

  1.2.7- Se ingresa contrasena de root.
   
  ![Comando tree](contrasenaroot_Debian.png "Comando tree")

  1.2.8- Se ingresa nombre, usuario y contrasena de nuevo usuario.
 
  ![Comando tree](NuevoU1_debian.png "Comando tree")

  ![Comando tree](NuevoU2_debian.png "Comando tree")

  ![Comando tree](NuevoU3_debian.png "Comando tree")

  1.2.9- Se selecciona metodo de particion del disco.
   
  ![Comando tree](MetodoParticion_debian.png "Comando tree")

  1.2.10- Se selecciona disco a particionar.
   
  ![Comando tree](DiscoParticion_debian.png "Comando tree")

  1.2.11- Se selecciona esquema de particion.
   
  ![Comando tree](Esquemaparticion_debian.png "Comando tree")

  1.2.12- Se selecciona no escanear otro disco.
   
  ![Comando tree](CD_Debian.png "Comando tree")

  1.2.13- Se seleccionan repositorios, para el packet manager.
   
  ![Comando tree](Repos1_Debian.png "Comando tree")

  ![Comando tree](Repos2_Debian.png "Comando tree")

  1.2.14- Se deja en blanco el proxy Http.
   
  ![Comando tree](proxy.png "Comando tree")
 
  1.2.15- Se selecciona software a instalar.
  
  Al seleccionar ssh server, se instalara ssh. Y al seleccionar Web Server, se instalara Apache2.

  ![Comando tree](software_debian.png "Comando tree")

  1.2.16- Se instala el GRUB en disco duro donde instalamos.
   
  ![Comando tree](GRUB_Debian.png "Comando tree")

  ![Comando tree](GRUB2_Debian.png "Comando tree")

  1.2.17- Se finaliza instalacion, donde se reiniciara automatica el Servidor.
  
  ![Comando tree](Finalizacion_debian.png "Comando tree")

2- Actualizamos los paquetes de los repositorios. 

  2.1 Usamos el usuario root.
  
  2.2 Corremos los siguientes comandos para actualizar los paquetes.
    
    # apt-get update
    # apt-get upgrade
    
3- Instalacion de los pogramas para los protocolos SMTP, POP3, IMAP.
  
  En nuestro caso se instala poxfix para el SMTP y Courier para POP3 y IMAP.
  
  3.1- Los siguientes son los pasos para la instalacion de postfix.
  
  3.1.1- Se corre el siguiente comando para instalar postfix.
    
    # apt-get install postfix
    
  3.1.2- Aparecera una ventana donde elegiremos la opcion "Internet site".

  ![Comando tree](Conf_Postfix.png "Comando tree")

  3.1.3- Luego otra ventana donde ingresaremos nuestro dominio.

  ![Comando tree](dominio_Postfix.png "Comando tree")

  3.2- Pasos para configurar postfix.
  
  3.2.1- Modificamos el archivo main.cf de postfix
    
    # nano /etc/postfix/main.cf
  
  En donde se modificara la linea: 
    inet_protocols = ipv4
    Ademas se insertara la linea: 
    home_mailbox = Maildir/
    Y guardamos y salimos con: ^O, Enter, ^X
    
  3.3- Los siguientes son los pasos para la instalacion de courier.
  
  3.3.1- Se corre el siguiente comando para instalar courier (pop y imap).
    
    # apt-get install courier-imap courier-pop

  3.3.2- Aparecera una ventana donde daremos si a la creacion de directorios y damos ok a las siguintes 2 ventanas.

  ![Comando tree](Directorios_Courier.png "Comando tree")

4- Instalacion de Squirrelmail como aplicacion para enviar y recibir correos.Existen 2 opciones:
  
  @Opcion1:

  4.1- Los siguientes son los pasos para obtener squirrelail.
  
  4.1.1- Se ingresa a la carpeta tmp.
      
    # cd /tmp
    
  4.1.2- Se descarga squirrelmail.
    
    # wget http://downloads.sourceforge.net/project/squirrelmail/stable/1.4.22/squirrelmail-webmail-1.4.22.zip
    
  4.1.3- Se instala unzip para descomprimir.
    
    # apt-get install unzip
    
  4.1.4- Se descomprime archivo .zip de squirrelmail.
    
    # unzip squirrelmail-webmail-1.4.22.zip
   
  4.1.5- Se mueve carpeta de squirrel.
    
    # mv squirrelmail-webmail-1.4.22/ /var/www/html/mail
    
  4.1.6- Se da permisos, para que apache interactue con squirrelmail.
    
    # chown -R www-data:www-data /var/www/html/mail
    
  4.1.7- Se copia la configuracion de archivo config_default.php a config.php.
    
    # cp /var/www/html/mail/config/config_default.php  /var/www/html/mail/config/config.php

  4.1.8- Se ingresa con nano a archivo config.php para realizar cambios de configuracion
    
    # nano /var/www/html/mail/config/config.php
      
  -Se modifican siguientes lineas
    
    domain = 'infocom.isw612.xyz';
    data_dir = '/var/www/html/mail/data/';
    attachment_dir = '/var/www/html/mail/attach/';

  4.1.9- Se crea carpeta attach
    
    # mkdir /var/www/html/mail/attach/

  4.1.10- Podemos ejecutar el siguiente comando una vez más para garantizar que los permisos se actualicen en el directorio
      
    # chown -R www-data:www-data /var/www/html/mail
  
  
  @Opcion2
    
  4.1- Los siguientes son los pasos para obtener squirrelail.
      
  4.1.1- Se insertan nuevas lineas de repositorios de Jessie en directorio "source.list".
    
    # nano /etc/apt/sources.list

  Ingresamos los siguintes repositorios:
        
    deb http://ftp.debian.org/debian/ jessie main contrib non-free
    deb-src http://ftp.debian.org/debian/ jessie main
    deb http://security.debian.org/ jessie/updates main contrib non-free
    deb-src http://security.debian.org/ jessie/updates main
    
  4.1.2- Se realiza un update y upgrade, para recargar los nuevos paquetes.
        
    # apt-get update
    # apt-get upgrade

  En caso de existir problema con el upgrade se utiliza:
    
    # apt-get --fix-broken install
    
  4.1.3- Se instala squirrelmail.
        
    # apt-get install squirrelmail
    
  4.1.4- Vamos a correr el conf.pl paraconfigurar el squirrelmail 
        
    # cd etc/squirrelmail
    # ./conf.pl

  ![Comando tree](Settings_squirrel.png "Comando tree")

  4.1.4.1- Entramos a la configuracion predefinida y seleccionamos el imap server.

  ![Comando tree](imap_squirrel.png "Comando tree")

  4.1.4.2- Ingresamos a server settings y modificamos el dominio.

  ![Comando tree](SeverSetting_squirrel.png "Comando tree")

  ![Comando tree](Domain_squirrel.png "Comando tree")

  4.1.4.3- Guardamos con S y salimos con Q.

  4.1.5- Ingresamos a /var/www/html y corremos el comando ln -s /usr/share/squirrelmail webmail
    
    # cd var/www/html
    # ln -s /usr/share/squirrelmail webmail

  4.1.6- Instalamos el mailutils para enviar al usuario el primer correo.
    
    # apt-get install mailutils
    
  4.1.7- Para enviar el primer correo corremos el comando
    
    # mail user
    
  En este ponemos damos enter ente opciones rellenando lo solicitado segun se prefiera, al final del correo damos ^D.

  ![Comando tree](Mail_mailutils.png "Comando tree")

5- Con esto ya tenemos configurado nuestro servidor   